provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

resource "helm_release" "my_app1" {
  name    = "azure-function-example-csharp8"
  chart   = var.helm_chart_path
  version = "1.0.0"

  set {
    name  = "image.repository"
    value = var.docker_image
  }
}
