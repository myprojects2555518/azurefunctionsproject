using Microsoft.Extensions.Hosting;
using Function.Domain.Services;
using Function.Domain.Services.HttpClients;
using Function.Domain.Providers;
using Function.Domain.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Azure.Functions.Worker.Extensions.OpenApi.Extensions;
using Microsoft.Azure.Functions.Worker.Configuration;
using System;

namespace azure_function_example_csharp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var host = new HostBuilder()
                    .ConfigureFunctionsWorkerDefaults(worker => worker.UseNewtonsoftJson())
                    .ConfigureServices((hostContext, services) =>
                    {
                        services.AddScoped<IFinhubDataMapper, FinhubDataMapper>();
                        services.AddScoped<IStockDataProvider, FinhubProvider>();
                        services.AddScoped<IHttpHelper, HttpHelper>();
                        services.AddHttpClient<FinhubHttpClient>();
                    })
                    .ConfigureAppConfiguration((hostContext, config) =>
                    {
                        // Add any additional configuration here if needed
                    })
                    .ConfigureServices(services =>
                    {
                        services.Configure<WorkerOptions>(options =>
                        {
                            options.UseGrpcServer(new Uri("http://localhost")); // Use localhost for local development
                        });
                    })
                    .Build();

                host.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
                Console.WriteLine($"==================================");
                Console.WriteLine($"Stack Trace: {ex.StackTrace}");
            }
        }
    }
}
